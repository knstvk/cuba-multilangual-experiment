package com.company.multilangual.approach1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;

@Listeners({"multilangual_LocalizableEntityMessageListener", "multilangual_LocalizableEntityMessageListener"})
@NamePattern("#getCurrentLocaleMessage|currentLocaleMessage")
@Table(name = "MULTILANGUAL_LOCALIZABLE_MESSAGE")
@Entity(name = "multilangual$LocalizableMessage")
public class LocalizableMessage extends StandardEntity {
    private static final long serialVersionUID = -9077224899403719439L;

    @OnDeleteInverse(DeletePolicy.DENY)
    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "localizableMessage")
    protected List<MessageTranslation> translations = new ArrayList<>();

    @Transient
    @MetaProperty
    protected String currentLocaleIsocode;


    public String getCurrentLocaleIsocode() {
        return currentLocaleIsocode;
    }


    public void setTranslations(List<MessageTranslation> translations) {
        this.translations = translations;
    }

    public List<MessageTranslation> getTranslations() {
        return translations;
    }

    @MetaProperty(related = "translations")
    public String getCurrentLocaleMessage() {
        Optional<MessageTranslation> current = translations.stream().filter(t -> t.getLanguage().getIsoCode().equals(currentLocaleIsocode)).findFirst();
        return current.isPresent() ? current.get().getMessage() : "Undefined";
    }

    public void setCurrentLocaleIsocode(String currentLocaleIsocode) {
        this.currentLocaleIsocode = currentLocaleIsocode;
    }
}