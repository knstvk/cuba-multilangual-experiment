package com.company.multilangual.approach1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.company.multilangual.language.Language;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

@NamePattern("%s|message")
@Table(name = "MULTILANGUAL_MESSAGE_TRANSLATION")
@Entity(name = "multilangual$MessageTranslation")
public class MessageTranslation extends StandardEntity {
	private static final long serialVersionUID = -8652870869079877576L;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "LANGUAGE_ISOCODE")
	protected Language language;

	@Column(name = "MESSAGE")
	protected String message;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "LOCALIZABLE_MESSAGE_ID")
	protected LocalizableMessage localizableMessage;

	public MessageTranslation() {
	}

	public MessageTranslation(Language language, String message) {
		this.language = language;
		this.message = message;
	}

	public void setLocalizableMessage(LocalizableMessage localizableMessage) {
		this.localizableMessage = localizableMessage;
	}

	public LocalizableMessage getLocalizableMessage() {
		return localizableMessage;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Language getLanguage() {
		return language;
	}
}