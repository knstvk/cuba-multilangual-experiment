package com.company.multilangual.language;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import com.haulmont.cuba.core.entity.BaseStringIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|isoCode")
@Table(name = "MULTILANGUAL_LANGUAGE")
@Entity(name = "multilangual$Language")
public class Language extends BaseStringIdEntity {
    private static final long serialVersionUID = 7537006221661883821L;

    @Id
    @Column(name = "ISOCODE", nullable = false, length = 2)
    protected String isoCode;

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    @Override
    public String getId() {
        return isoCode;
    }

    @Override
    public void setId(String id) {
        this.isoCode = id;
    }


}