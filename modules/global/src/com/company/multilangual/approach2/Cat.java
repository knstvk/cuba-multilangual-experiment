package com.company.multilangual.approach2;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.StandardEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Table(name = "MULTILANGUAL_CAT")
@Entity(name = "multilangual$Cat")
public class Cat extends StandardEntity {
    private static final long serialVersionUID = -6017886848778147518L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    protected String name;

    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTH_DATE")
    protected Date birthDate;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    @Column(name = "DESCRIPTION", columnDefinition = "varchar(410)")
    @Convert(converter = LocalizedStringToJsonConverter.class)
    @MetaProperty(datatype = "localizedString")
    protected LocalizedString favouriteFood;


    public LocalizedString getFavouriteFood() {
        return favouriteFood;
    }

    public void setFavouriteFood(final LocalizedString favouriteFood) {
        this.favouriteFood = favouriteFood;
    }
}