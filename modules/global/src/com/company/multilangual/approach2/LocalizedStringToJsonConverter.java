package com.company.multilangual.approach2;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Converter
public class LocalizedStringToJsonConverter implements AttributeConverter<LocalizedString, String> {
	@Override
	public String convertToDatabaseColumn(final LocalizedString localized) {
		final Gson gson = new GsonBuilder().create();
		return gson.toJson(localized);
	}

	@Override
	public LocalizedString convertToEntityAttribute(final String json) {
		final Gson gson = new GsonBuilder().create();
		return gson.fromJson(json, LocalizedString.class);
	}
}
