package com.company.multilangual.approach2;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.google.common.collect.Maps;

public class LocalizedString implements Serializable {
    private Map<Locale, String> translations = new HashMap<>();
    private Locale currentlocale;

    public LocalizedString(Map<Locale, String> translations, Locale locale) {
        currentlocale = locale;
        this.translations = Maps.newHashMap(translations);
    }

    public LocalizedString(Map<Locale, String> translations) {
        this(translations, null);
    }

    public String getCurrentLocaleMessage() {
        return translations.get(currentlocale);
    }

    public String getMessage(Locale locale) {
        return translations.get(locale);
    }

    public void setCurrentLocale(final Locale locale) {
        this.currentlocale = locale;
    }

    public Map<Locale, String> getTranslations() {
        return translations;
    }
}