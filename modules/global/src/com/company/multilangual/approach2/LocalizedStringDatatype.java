package com.company.multilangual.approach2;

import com.haulmont.chile.core.annotations.JavaClass;
import com.haulmont.chile.core.datatypes.Datatype;

import javax.annotation.Nullable;
import java.text.ParseException;
import java.util.Locale;

@JavaClass(LocalizedString.class)
public class LocalizedStringDatatype implements Datatype<LocalizedString> {

    @Override
    public String format(@Nullable Object value) {
        if (value == null)
            return "";
        return new LocalizedStringToJsonConverter().convertToDatabaseColumn((LocalizedString) value);
    }

    @Override
    public String format(@Nullable Object value, Locale locale) {
        return format(value);
    }

    @Nullable
    @Override
    public LocalizedString parse(@Nullable String value) throws ParseException {
        if (value == null)
            return null;
        return new LocalizedStringToJsonConverter().convertToEntityAttribute(value);
    }

    @Nullable
    @Override
    public LocalizedString parse(@Nullable String value, Locale locale) throws ParseException {
        return parse(value);
    }
}
