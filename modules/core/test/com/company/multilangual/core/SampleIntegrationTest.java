package com.company.multilangual.core;

import com.company.multilangual.MultilangualTestContainer;
import com.company.multilangual.approach2.Cat;
import com.company.multilangual.approach2.LocalizedString;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class SampleIntegrationTest {

    @ClassRule
    public static MultilangualTestContainer cont = MultilangualTestContainer.Common.INSTANCE;

    private Metadata metadata;
    private Persistence persistence;
    private DataManager dataManager;

    @Before
    public void setUp() throws Exception {
        metadata = cont.metadata();
        persistence = cont.persistence();
        dataManager = AppBeans.get(DataManager.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCat() {
        Cat cat = metadata.create(Cat.class);
        cat.setName("Tiger");
        cat.setBirthDate(new Date());

        Map<Locale, String> translations = new HashMap<>();
        translations.put(Locale.ENGLISH, "meat");
        translations.put(Locale.forLanguageTag("ru"), "мясо");
        LocalizedString favouriteFood = new LocalizedString(translations);
        cat.setFavouriteFood(favouriteFood);

        dataManager.commit(cat);

        Cat cat1 = dataManager.load(Cat.class).id(cat.getId()).one();
        assertEquals("meat", cat1.getFavouriteFood().getTranslations().get(Locale.ENGLISH));
    }
}