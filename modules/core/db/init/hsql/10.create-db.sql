-- begin MULTILANGUAL_LANGUAGE
create table MULTILANGUAL_LANGUAGE (
    ISOCODE varchar(2) not null,
    --
    primary key (ISOCODE)
)^
-- end MULTILANGUAL_LANGUAGE
-- begin MULTILANGUAL_LOCALIZABLE_MESSAGE
create table MULTILANGUAL_LOCALIZABLE_MESSAGE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    primary key (ID)
)^
-- end MULTILANGUAL_LOCALIZABLE_MESSAGE
-- begin MULTILANGUAL_MESSAGE_TRANSLATION
create table MULTILANGUAL_MESSAGE_TRANSLATION (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LANGUAGE_ISOCODE varchar(2) not null,
    MESSAGE varchar(255),
    LOCALIZABLE_MESSAGE_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end MULTILANGUAL_MESSAGE_TRANSLATION
-- begin MULTILANGUAL_DOG
create table MULTILANGUAL_DOG (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    BREED_ID varchar(36) not null,
    NAME varchar(255),
    BIRTH_DATE timestamp,
    --
    primary key (ID)
)^
-- end MULTILANGUAL_DOG
-- begin MULTILANGUAL_CAT
create table MULTILANGUAL_CAT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    BIRTH_DATE date,
    DESCRIPTION varchar(410),
    --
    primary key (ID)
)^
-- end MULTILANGUAL_CAT
