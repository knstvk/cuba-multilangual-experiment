package com.company.multilangual.core;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.core.listener.BeforeDetachEntityListener;
import com.haulmont.cuba.core.EntityManager;
import com.company.multilangual.approach1.LocalizableMessage;

@Component("multilangual_LocalizableEntityMessageListener")
public class LocalizableEntityMessageListener implements BeforeDetachEntityListener<LocalizableMessage> {

    @Inject
    private UserSessionSource userSessionSource;

    @Override
    public void onBeforeDetach(LocalizableMessage entity, EntityManager entityManager) {
        entity.setCurrentLocaleIsocode(userSessionSource.getLocale().getLanguage());

    }

}