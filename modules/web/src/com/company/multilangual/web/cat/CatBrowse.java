package com.company.multilangual.web.cat;

import com.company.multilangual.approach2.Cat;
import com.company.multilangual.web.MultilingualHelper;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.Component;

import javax.inject.Inject;

public class CatBrowse extends AbstractLookup {

    @Inject
    private MultilingualHelper multilingualHelper;

    public Component generateFavouriteFoodCell(Cat entity) {
        return multilingualHelper.generateCell(entity.getFavouriteFood());
    }
}