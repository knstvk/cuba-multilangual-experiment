package com.company.multilangual.web.cat;

import com.company.multilangual.approach2.Cat;
import com.company.multilangual.web.MultilingualHelper;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.data.Datasource;

import javax.inject.Inject;

public class CatEdit extends AbstractEditor<Cat> {

    @Inject
    private MultilingualHelper multilingualHelper;

    public Component generateFavouriteFoodField(Datasource datasource, String fieldId) {
        return multilingualHelper.generateField(datasource, fieldId);
    }
}