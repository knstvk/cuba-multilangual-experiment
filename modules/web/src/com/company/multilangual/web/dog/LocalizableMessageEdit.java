package com.company.multilangual.web.dog;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.company.multilangual.approach1.MessageTranslation;
import com.company.multilangual.language.Language;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.company.multilangual.approach1.LocalizableMessage;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;

public class LocalizableMessageEdit extends AbstractEditor<LocalizableMessage> {
	@Inject
	private Metadata metadata;
	@Inject
	private CollectionDatasource<Language, String> languagesDs;
	@Inject
	private CollectionDatasource<MessageTranslation, UUID> translationsDs;
	@Inject
	private Datasource<LocalizableMessage> localizableMessageDs;

	@Override
	protected void postInit() {
		languagesDs.refresh();
		addUndefinedTranslations();
	}


	@Override
	protected void initNewItem(final LocalizableMessage item) {
		super.initNewItem(item);
	}

	private void addUndefinedTranslations() {
		Collection<Language> allLanguages = getItem().getTranslations().stream().map(i -> i.getLanguage()).collect(Collectors.toList());
		languagesDs.getItems().stream().filter(i -> !allLanguages.contains(i)).forEach(this::addTranslation);
	}


	private void addTranslation(Language language) {
		MessageTranslation translation = metadata.create(MessageTranslation.class);
		translation.setMessage("Undefined");
		translation.setLanguage(language);
		getItem().getTranslations().add(translation);
		translation.setLocalizableMessage(getItem());
		translationsDs.addItem(translation);

	}
}