package com.company.multilangual.web;

import com.company.multilangual.approach2.Cat;
import com.company.multilangual.approach2.LocalizedString;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;

@Component
public class MultilingualHelper {

    @Inject
    private UserSessionSource userSessionSource;

    @Inject
    private ComponentsFactory factory;

    @SuppressWarnings("unchecked")
    public TextField generateField(Datasource datasource, String fieldId) {
        TextField textField = factory.createComponent(TextField.class);
        datasource.addItemChangeListener(itemChangeEvent -> {
            Object value = datasource.getItem().getValue(fieldId);
            LocalizedString locStr = value != null ? (LocalizedString) value : new LocalizedString(new HashMap<>());
            textField.setValue(locStr.getTranslations().get(userSessionSource.getLocale()));

            textField.addValueChangeListener(valueChangeEvent -> {
                String text = (String) valueChangeEvent.getValue();
                LocalizedString newLs = new LocalizedString(locStr.getTranslations());
                newLs.getTranslations().put(userSessionSource.getLocale(), text);
                datasource.getItem().setValue(fieldId, newLs);
            });
        });

        return textField;
    }

    public Table.PlainTextCell generateCell(LocalizedString localizedString) {
        String text = localizedString == null ? "" : localizedString.getTranslations().get(userSessionSource.getLocale());
        return new Table.PlainTextCell(text);
    }
}
